package br.com.saque.saque;

import java.util.HashMap;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;

public class RatesResponse {
	boolean success;
	int timestamp;
	String base;
	String date;
	HashMap<String, Double> rates;

	double Convert(double valor, String moeda){
	
		double valorCotacao;
		
		Gson gson = new Gson();
        String response = HttpRequest.get("http://data.fixer.io/api/latest?access_key=578ac533a860ac0f3d508e3dbad0ef57").body();
        RatesResponse fixer = gson.fromJson(response,RatesResponse.class);
		
        valorCotacao = fixer.rates.get(moeda);
        
		return valor *valorCotacao;
				
	}
	
	
}

package br.com.saque.saque;

interface  Customer {
	
	String name="";
	String username="";
	String password= "";
	double balance=0;
	
	char accountType='x';
	
		boolean withdraw(double amount)	;
		
		String toString();
		
		String getHeader();
}

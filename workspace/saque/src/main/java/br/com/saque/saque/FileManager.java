package br.com.saque.saque;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

public class FileManager {
	protected static HashMap<String, String> listaDadosCliente = new HashMap<String,String>();
	public String[] readFile(String CaminhoArquivo){
		ArrayList<String> listaDados = new ArrayList<>();
		
		
		Path path = Paths.get("accounts.csv");
		StringBuilder leitorArquivo = new StringBuilder();

		try {
			listaDados = (ArrayList<String>) Files.readAllLines(path);
			for(int i=0; i<listaDados.size(); i++) {
				String[] dados = listaDados.get(i).split(",");
				listaDadosCliente.put(dados[1], listaDados.get(i));
			}
			
			//impressao
			for(int i=1; i<listaDados.size();i++) {
				System.out.println(listaDados.get(i));
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String[] colunas = new String[5];
		
		for(String linha : listaDados) {
			colunas = linha.split(",");
	}
		return colunas;
	}
}

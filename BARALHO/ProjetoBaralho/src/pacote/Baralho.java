package pacote;
import java.util.Random;

public class Baralho {

	private Carta[] baralho =new Carta[51];
	
	/*1 ao 12
	 * 
	 * 1 = A
	 * 10 = 
	 * 11 = J =10
	 * 12 = Q =10
	 * 13 = K =10 
	 * 
	 * Baralho
	 * 	criarEPopularBaralho
	 * 	
	 * 
	 * Classe Main = Jogador
	 * 	Jogador representado pela classe Main
	 * 
	 * Pontuacao
	 *  
	 */
	
	public void criarEPopularBaralho() {
		Random aleatorio = new Random();
		int valorSorteado ;
		
		for(int c=0; c<baralho.length; c++) {
			valorSorteado = aleatorio.nextInt(13);
			
			//ignora valores com 0
			while (valorSorteado ==0) {
				valorSorteado =aleatorio.nextInt(13);
			}
			

				//insere numero na Carta
					baralho[c] = new Carta("",0,0);
					baralho[c].setNumero(valorSorteado);
					baralho[c].setValorFinal(valorSorteado);
		
				//insere naipe
					baralho[c].setNaipe("n");			
							
		}
		
		
	}
	
	public void imprimeBaralho() {
		
		for(int c=0; c<baralho.length; c++) {
			System.out.print(baralho[c].getValorFinal());
			System.out.print(baralho[c].getNaipe());
			System.out.print(" ");
		}
	}
	
	public Carta darCarta() {
		Random aleatorio = new Random();
		int numero = aleatorio.nextInt(51);
		
		return baralho[numero];

	}
	
	
	
}
package pacote;

public class Carta {
	
	private String	naipe;
	private int		numero;
	private int 	valorFinal;
	
	Carta(String n, int nr, int vr){
		naipe = n;
		numero =nr;
		valorFinal=vr;
	}
	
	
	public String getNaipe (){		
		return this.naipe;
	}
	
	public int getNumero (){		
		return this.numero;
	}
	
	public int getValorFinal (){		
		return this.valorFinal;
	}


	public void setNaipe (String x){		
		this.naipe = x;
	}
	
	public void setNumero (int numeroRecebido){		
		this.numero = numeroRecebido;
	}
	
	public void setValorFinal (int valorFinalRecebido){		
		
		if((valorFinalRecebido >=11) && (valorFinalRecebido <=13)) {
			valorFinalRecebido =10;
		}
		this.valorFinal = valorFinalRecebido; 
	}


}
